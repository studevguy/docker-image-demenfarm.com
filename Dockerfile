FROM node:10.18-alpine3.9
MAINTAINER  Sang Dao <sysangtiger@gmail.com>

RUN apk add --no-cache bash \
    build-base \
    supervisor=3.3.4-r1 \
    tzdata=2019c-r0 \
    nginx=1.14.2-r4 && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /var/log/supervisor && \
    mkdir -p /etc/nginx/sites-enabled/ && \
    mkdir -p /var/www/html/

ADD .docker/conf/supervisord.conf /etc/supervisord.conf
ADD .docker/conf/nginx.conf /etc/nginx/nginx.conf
ADD .docker/conf/nginx-site.conf /etc/nginx/sites-enabled/default.conf

# Add Scripts
ADD .docker/scripts/start.sh /start.sh
ADD .docker/scripts/build_ui.sh /build_ui.sh
ADD .docker/scripts/build_api.sh /build_api.sh
ADD .docker/scripts/ws.sh /go_to_ws.sh

# copy in code
ADD .docker/errors /var/www/errors/

EXPOSE 80

CMD ["/bin/bash","/start.sh"]

